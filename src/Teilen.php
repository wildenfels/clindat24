<?php
/**
 * File Teilen.php gets a clean integer and divides it by 100. Result is
 displayed rounded to two decimals.
 *
 * PHP version 8
 *
 * @category  PHP
 * @package   Clindat24
 * @author    Nadine Katzke <nkatzke@aol.com>
 * @copyright 2021 Clindat24
 * @license   BSD-3 https://opensource.org
 * @link      http://clindat.mibeg-cms.de/
 */

/**
 * Class Teilen.php gets a clean integer and divides it by 100. Result is
 displayed rounded to two decimals.
 *
 * PHP version 8
 *
 * @category  PHP
 * @package   Clindat24
 * @author    Nadine Katzke <nkatzke@aol.com>
 * @copyright 2021 Clindat24
 * @license   BSD-3 https://opensource.org
 * @link      http://clindat.mibeg-cms.de/
 */
class Teilen
{
    /**
     * Number for division, $zahl.
     *
     * @var    float
     * @access private
     */
    private $zahl = 0; //Eigenschaft $zahl als Zahl festlegen
    
    /**
     * Function durch100 divides incoming number by 100 and rounds to two decimals.
     Returns number as float.
     *
     * @param integer $zahl_ext (incoming number).
     *
     * @return float
     * @access public
     */
    public function durch100($zahl_ext)
    {
        $this->zahl = $zahl_ext; //eingehende Zahl auslesen
        
        $this->zahl = round(($this->zahl/100), 2); //durch 100 teilen und runden
        
        return $this->zahl; //Rückgabe des Werts von Zahl
    }//end durch100
}//end class


<?php
/**
 * File G3.php encapsulates the customer information.
 *
 * PHP version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Cihan Guemues, Elena Pestova, Jonatan Wörle <dummy@mail.de>
 * @copyright 2021 Mibeg
 * @license   BSD-3 https://opensource.org
 * @link      http://clindat.mibeg-cms.de/20210114/...
 */

/**
 * Class G3
 *
 * PHP version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Cihan Guemues, Elena Pestova, Jonatan Wörle <dummy@mail.de>
 * @copyright 2021 Mibeg
 * @license   BSD-3 https://opensource.org
 * @link      http://clindat.mibeg-cms.de/20210114/...
 */

class G3
{
    /**
     * First number variable, $var1.
     *
     * @var    integer
     * @access private
     */
    private $var1 = 0;
    /**
     * Second number variable, $var2.
     *
     * @var    integer
     * @access private
     */
    
    private $var2 = 0;
    
    /**
     * Function setvar1 sets $var1 to variable.
     *
     * @param int $var1 of var1
     *
     * @return integer
     * @access public
     */
    public function setvar1($var1)
    {
        $this->var1 = Intval($var1);
    } //end function
    
    /**
     * Function setvar2 sets $var2 to variable.
     *
     * @param int $var2 of var2
     *  
     * @return integer
     * @access public
     */
    public function setvar2($var2)
    {
        $this->var2 = Intval($var2);
    } //end function
    /**
     * Function getvar1 returns value of $var1.
     *
     * @return integer
     * @access public
     */
    public function getvar1()
    {
        return $this->var1;
    } //end function
    /**
     * Function getvar2 returns value of $var2.
     *
     * @return integer
     * @access public
     */
    public function getvar2()
    {
        return $this->var2;
    } //end function
} //end class
?>
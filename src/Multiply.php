<?php
/**
 * Datei Multiply.php multipliziert mit sich selbst
 *
 * PHP Version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Alex Grämer <alex.graemer@gmx.net>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de/20210113/graemer/src/
 */

/**
 * Klasse Multiply multipliziert mit sich selbst
 *
 * PHP Version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Alex Grämer <alex.graemer@gmx.net>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de/20210113/graemer/src/
 */
class Multiply
{
    /**
     * Das ist die leere Variable für den input: float1
     *
     * @var    float
     * @access private
     */
    private $float1 = "";
    
    /**
     * Das ist der Zwischenspeicher (ohne Runden): float2
     *
     * @var    float
     * @access private
     */
    private $float2 = "";
    
    /**
     * Das ist was als return-value zurückgegeben wird: result
     *
     * @var    float
     * @access private
     */
    private $result = "";


    /**
     * Method multi
     *
     * @param input $input float von aussen
     *                     float1 $float1 gestippter float
     *                     float2 $float2 resultat multiplikation mit sich selbst
     * 
     * @return result gerundeter float return
     * @access public
     */
    public function multi($input)
    {
        $this->float1 = strip_tags($input);
        $this->float2 = $this->float1 * $this->float1;
        $this->result = round($this->float2, 2);
        return $this->result;
    }//end multi
}//end class

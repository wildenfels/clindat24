<?php
/**
 * File Division.php teilt einen Integer durch einen Float.
 *
 * PHP Version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Sarah, Michael, Philip <my@email.de>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de/20210114/clindat24
 */

/**
 * Class Division.php teilt einen Integer durch einen Float.
 *
 * PHP Version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Sarah, Michael, Philip <my@email.de>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de/20210114/clindat24
 */
 
class Division
{
    /**
     * Method fncQuotient
     *
     * @param integer $intDividend von intDividend
     * @param float   $fltDivisor  von fltDivisor
     *
     * @return float (0.00)
     * @access public
     */
    public function fncQuotient($intDividend, $fltDivisor)
    {
        return number_format($intDividend / $fltDivisor, 2);
    }
}
